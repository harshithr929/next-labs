# Next labs

## About Project

- Project has two modules 
  - Administrator 
  - Customer

- Administrator has rights to upload a Task for apps that user downloads 

- Customer logins into the application and uploads screenshot of the application and gets points as reward

# Administrator routes

```
  login           - 0.0.0.0:8000/Administrator/
  Signup          - 0.0.0.0:8000/admin_signup/
  Admin_details   - 0.0.0.0:8000/Administrator/admin_details/
  Home page       - 0.0.0.0:8000/Administrator/home/
  List task       - 0.0.0.0:8000/Administrator/listapps/
  Add Task        - 0.0.0.0:8000/Administrator/create_task/
  Update Task     - 0.0.0.0:8000/Administrator/updateapps/<str:pk>/
  Update Profile  - 0.0.0.0:8000/Administrator/updateprofile/
  Delete Task     - 0.0.0.0:8000/Administrator/deleteapp/<str:pk>/
```

# Customer routes


```
  login           - 0.0.0.0:8000/Customer/customer_login/
  Signup          - 0.0.0.0:8000/
  Customer_details- 0.0.0.0:8000/Customer/customer_details/
  Home page       - 0.0.0.0:8000/Customer/home/
  List task       - 0.0.0.0:8000/Customer/listtask/
  Add screenshot  - 0.0.0.0:8000/Customer/addscreenshot/<str:pk>/
  Update Profile  - 0.0.0.0:8000/Customer/updatecustomerprofile/
  Completed Task  - 0.0.0.0:8000/Customer/completedtask/
```

## About Backend

- In this project we have 6 Database tables 
  - UserAccountManager
  - UserAccount
  - Customer
  - Administrator
  - CustomerDetails
  - AdministratorDetails
  - Task
  
- UserAccountManager model is a BaseUserManager used to create user and superuser

- UserAccount model is a AbstractBaseUser which contains details regarding user like email, username, password, is_staff
  - created two choices in UserAccount model 
    - Administrator (is_administrator)
    - Customer      (is_customer)

- Customer model is a extension of UserAccount and is_customer is set as True

- Administrator model is a extension of UserAccount and is_administrator is set as True

- CustomerDetails model inherent property of CustomerModel which contains
  - Username
  - First Name
  - Last Name
  - Phone
  - Date of Birth
  - address
  - Total points

- AdministratorDetails model inherent property of Administrator which contains
  - Username
  - First Name
  - Last Name
  - Phone
  - Date of Birth
  - address

- Task model contains details about task which contains
  - appname
  - categoryname
  - subcategoryname
  - appsubcategory
  - points
  - screenshot
  - work
  - completed_by    (ForeignKey to Customer model)

- OPEN API'S 
  ```
  - 0.0.0.0:8000/api/task_list_api/                                 To Fetch all Task
  - 0.0.0.0:8000/api/task_create_api/                               To Create Task
  - 0.0.0.0:8000/api/task_update_api/<str:pk>/                      To Update Task using Task ID
  - 0.0.0.0:8000/api/task_delete_api/<str:pk>/                      To Delete Task using Task ID
  - 0.0.0.0:8000/customer/task_update_screenshot_api/<str:pk>/      To Update screenshot to Task 
  ```

## Frontend

  - HTML
  - CSS 

## tools used
  
  - Docker Container is used to run this project

  - Framework
    - Django and Django rest-framework 


## Steps to Run the Project(only for win/linux):

- Install docker and docker-compose *if not installed*
  - [docker-installation](https://docs.docker.com/compose/install/)

- Pull git repo to local machine 
```
  - git clone https://gitlab.com/harshithr929/next-labs.git
```

- open terminal and Change directory to main
```
  - cd next-labs/Task2
```

- run docker-compose 
```
  - docker-compose up
```

- Open any browser and type :
```
  - 0.0.0.0:8000/
```

- To stop docker type in terminal
```
  - ctrl+c
```
- Migrate to Database
```
  - docker-compose run web python manage.py makemigrations
  - docker-compose run web python manage.py migrate
```

- To create a superuser in django
```
  - docker-compose web run python manage.py createsuperuser
```

- To Turn down docker
```
  - docker-compose down
```


